import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLogin: boolean = false;
  userName: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) {
  }

  ngOnInit() {
    this.authService.checkAuth().subscribe(auth => {
      if (auth) {
        this.isLogin = true;
        this.userName = auth.email;
      } else {
        this.userName = '';
        this.isLogin = false;
      }
    });
  }

  logout() {
    this.authService.logout()
      .then(() => {
        this.router.navigate(['/login']);

        this.flashMessage.show('We\'re sorry to see you go!', {
          cssClass: 'alert-warning',
          showCloseBtn: true,
          closeOnClick: true,
          timeOut: 4000
        });
      })
      .catch(err => {
        console.log(err);

        this.flashMessage.show(err.message, {
          cssClass: 'alert-danger',
          showCloseBtn: true,
          closeOnClick: true,
          timeOut: 4000
        });
      });
  }
}
