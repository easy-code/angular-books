import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;

  @ViewChild('form') form;

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) {
  }

  ngOnInit() {
    // Check auth state
    this.authService.checkAuth().subscribe(auth => {
      if (auth) {
        this.router.navigate(['/panel']);
      }
    });
  }

  onSubmit() {
    this.authService.register(this.email, this.password)
      .then(user => {
        this.form.reset();
        this.router.navigate(['/panel']);

        this.flashMessage.show('Thanks for being awesome!', {
          cssClass: 'alert-success',
          showCloseBtn: true,
          closeOnClick: true,
          timeOut: 4000
        });
      })
      .catch(err => {
        console.log(err);

        this.flashMessage.show(err.message, {
          cssClass: 'alert-danger',
          showCloseBtn: true,
          closeOnClick: true,
          timeOut: 4000
        });
      });
  }
}
