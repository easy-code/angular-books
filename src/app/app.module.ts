import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireAuthModule} from 'angularfire2/auth';

import {IdService} from './services/id.service';
import {BooksService} from './services/books.service';
import {AuthService} from './services/auth.service';

import {environment} from '../environments/environment';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';

import {PanelComponent} from './components/panel/panel.component';
import {AddBookComponent} from './components/add-book/add-book.component';
import {EditBookComponent} from './components/edit-book/edit-book.component';
import {AboutComponent} from './components/about/about.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';

import {FlashMessagesModule} from 'angular2-flash-messages';

@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    AddBookComponent,
    EditBookComponent,
    AboutComponent,
    NotFoundComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FlashMessagesModule.forRoot(),
  ],
  providers: [BooksService, IdService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
