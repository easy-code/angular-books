// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAGqeMDamNnS6kjRlTiW_tus3ynlvLSzxU',
    authDomain: 'books-store-43b9b.firebaseapp.com',
    databaseURL: 'https://books-store-43b9b.firebaseio.com',
    projectId: 'books-store-43b9b',
    storageBucket: 'books-store-43b9b.appspot.com',
    messagingSenderId: '854155643537'
  }
};
